package org.silk.codingame.easy.asciiart;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int l = in.nextInt();
        in.nextLine();
        int h = in.nextInt();
        in.nextLine();
        String t = in.nextLine();
        char[] chars = t.toUpperCase().toCharArray();
        for (int i = 0; i < h; i++) {
            String row = in.nextLine();
            StringBuilder out = new StringBuilder();
            for (char c : chars) {
                int start;
                if (c < 'A' || c > 'Z') {
                    start = 26;
                } else {
                    start = c - 'A';
                }
                start *= l;
                out.append(row.substring(start, start + l));
            }
            System.out.println(out);

        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

    }
}