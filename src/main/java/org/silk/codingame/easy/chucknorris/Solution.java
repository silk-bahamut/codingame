package org.silk.codingame.easy.chucknorris;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String m = in.nextLine();
        char[] cArray = m.toCharArray();

        StringBuilder out = new StringBuilder();
        Character last = null;
        for (char c : cArray) {
            String s1 = String.format("%8s", Integer.toBinaryString(((byte) c) & 0xFF)).replace(' ', '0');
            char[] bits = s1.toCharArray();
            for (int j = 1; j < bits.length; j++) {
                char b = bits[j];
                if (last == null || b != last) {
                    if (last != null) {
                        out.append(" ");
                    }
                    last = b;
                    if (b == '0') {
                        out.append("0");
                    }
                    out.append("0 ");
                }
                out.append("0");
            }
        }
        System.out.println(out);
    }
}