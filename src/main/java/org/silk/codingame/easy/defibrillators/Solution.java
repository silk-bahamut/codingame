package org.silk.codingame.easy.defibrillators;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String lon = in.next();
        in.nextLine();
        String lat = in.next();
        in.nextLine();
        int n = in.nextInt();
        in.nextLine();
        double min = 0;
        String name = null;
        for (int i = 0; i < n; i++) {
            String defib = in.nextLine();
            String[] strs = defib.split(";");
            double dist = compute(lon, lat, strs[4], strs[5]);
            if (name == null || dist < min) {
                min = dist;
                name = strs[1];
            }
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(name);
    }

    public static double compute(String lon, String lat, String dlon, String dlat) {
        double longit = Double.parseDouble(lon.replace(',', '.'));
        double latit = Double.parseDouble(lat.replace(',', '.'));
        double deflong = Double.parseDouble(dlon.replace(',', '.'));
        double deflat = Double.parseDouble(dlat.replace(',', '.'));

        double x = (deflong - longit) * Math.cos((latit + deflat) / 2.0);
        double y = (deflat - latit);
        return Math.sqrt(x * x + y * y) * 6371;

    }
}