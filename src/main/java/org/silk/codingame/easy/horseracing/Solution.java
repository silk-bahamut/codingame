package org.silk.codingame.easy.horseracing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        List<Integer> lst = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int pi = in.nextInt();
            lst.add(pi);
        }
        Collections.sort(lst);
        int min = 10000000;
        Integer last = null;
        for (int i : lst) {
            if (last != null && (i - last) < min) {
                min = i - last;
            }
            last = i;
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(min);
    }
}