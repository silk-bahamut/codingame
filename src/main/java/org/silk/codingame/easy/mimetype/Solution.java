package org.silk.codingame.easy.mimetype;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // Number of elements which make up the association table.
        in.nextLine();
        int q = in.nextInt(); // Number Q of file names to be analyzed.
        in.nextLine();
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            String eXT = in.next(); // file extension
            String mT = in.next(); // MIME type.
            in.nextLine();
            map.put(eXT.toLowerCase(), mT);
        }
        for (int i = 0; i < q; i++) {
            String fname = in.nextLine(); // One file name per line.
            if (fname.lastIndexOf('.') > -1) {
                String ext = fname.substring(fname.lastIndexOf('.') + 1, fname.length()).toLowerCase();
                String out = map.get(ext);
                System.out.println(out == null ? "UNKNOWN" : out);
            } else {
                System.out.println("UNKNOWN");
            }

        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        // For each of the Q filenames, display on a line the corresponding MIME type. If there is no corresponding type, then display UNKNOWN.
    }
}