package org.silk.codingame.easy.skynet;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int road = in.nextInt(); // the length of the road before the gap.
        int gap = in.nextInt(); // the length of the gap.
        int platform = in.nextInt(); // the length of the landing platform.

        System.err.println("Road:" + road);
        System.err.println("Gap:" + gap);
        System.err.println("Platform:" + platform);
        // game loop
        while (true) {
            int speed = in.nextInt(); // the motorbike's speed.
            int coordX = in.nextInt(); // the position on the road of the motorbike.
            System.err.println("Speed:" + speed);
            System.err.println("coordX:" + coordX);
            if (coordX > road || speed > gap + 1) {
                System.out.println("SLOW");
            } else if (coordX < road && speed <= gap) {
                System.out.println("SPEED");
            } else if (coordX == road - 1) {
                System.out.println("JUMP");
            } else {
                System.out.println("WAIT");
            }
            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");
        }
    }
}