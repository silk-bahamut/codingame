package org.silk.codingame.easy.temperature;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the number of temperatures to analyse
        in.nextLine();
        String temps = in.nextLine(); // the N temperatures expressed as integers ranging from -273 to 5526

        int out = 0;
        int absOut = 10000;
        if (n > 0) {
            for (String s : temps.split(" ")) {
                int i = Integer.parseInt(s);
                if (Math.abs(i) < absOut) {
                    absOut = Math.abs(i);
                    out = i;
                } else if (Math.abs(i) == absOut && i > 0) {
                    out = i;
                }
            }
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(out);
    }
}