package org.silk.codingame.easy.thor;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * ---
 * Hint: You can use the debug stream to print initialTX and initialTY, if Thor seems not follow your orders.
 */
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int lightX = in.nextInt(); // the X position of the light of power
        int lightY = in.nextInt(); // the Y position of the light of power
        int initialTX = in.nextInt(); // Thor's starting X position
        int initialTY = in.nextInt(); // Thor's starting Y position

        int thorX = initialTX;
        int thorY = initialTY;
        // game loop
        while (true) {
            int remainingTurns = in.nextInt();
            String st = "";
            if (thorY < lightY) {
                st += "S";
                thorY++;
            } else if (thorY > lightY) {
                st += "N";
                thorY--;
            }
            if (thorX < lightX) {
                st += "E";
                thorX++;
            } else if (thorX > lightX) {
                st += "W";
                thorX--;
            }

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");

            System.out.println(st); // A single line providing the move to be made: N NE E SE S SW W or NW
        }
    }
}