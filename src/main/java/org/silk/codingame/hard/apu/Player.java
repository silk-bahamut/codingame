import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

/**
 * The machines are gaining ground. Time to show them what we're really made of...
 **/
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int width = in.nextInt();
        in.nextLine();
        int height = in.nextInt();
        in.nextLine();
        Queue<Cell> queue = buildQueue(in, width, height);
        while (!queue.isEmpty()) {
            Cell c = queue.poll();
            System.err.println(String.format("%s polled", c));
            if (c.isSolved()) {
                System.err.println(String.format("%s solved", c));
                for (Cell link : c.possibleLinks.keySet()) {
                    updateLinkAndQueue(queue, c, link);
                }
            } else {
                System.err.println(String.format("%s not solved", c));
                for (Map.Entry<Cell, Integer> en : c.possibleLinks.entrySet()) {
                    Cell link = en.getKey();
                    boolean updated = updateLinkAndQueue(queue, c, link);
                    if (updated) {
                        break;
                    }
                }
            }
            queue.add(c);
        }
    }

    /**
     * Check if the link between two cells is full
     * Remove the cell from the queue
     * Add the link
     * If the other cell needs more link, put it back in the queue
     */
    private static boolean updateLinkAndQueue(Queue<Cell> queue, Cell c, Cell other) {
        int amount = c.canLinkTo(other);
        if (amount > 0) {
            if (!c.isSolved()) {
                amount = 1;
            }
            System.err.println(String.format("%s can link to %s for %s", c, other, amount));
            queue.remove(other);
            System.out.println(String.format("%s %s %s %s %s", c.x, c.y, other.x, other.y, amount));
            other.addLink(c, amount);
            queue.add(other);
        } else {
            System.err.println(String.format("%s cannot link to %s", c, other));
            System.err.println(String.format("o.amountNeeded=%s, amountNeeded=%s, nbr of links=%s:%s", other.amountNeeded, c.amountNeeded, c.possibleLinks.get(other), other.possibleLinks.get(c)));
        }
        return amount > 0;
    }

    /**
     * Build a priority queue of the nodes that needs to be linked
     */
    private static Queue<Cell> buildQueue(Scanner in, int width, int height) {
        Map<Integer, Map<Integer, Cell>> nodes = intiMap(height, width);
        List<Cell> cells = new ArrayList<>();
        Cell[] up = new Cell[width];
        Cell last = null;
        // parse input and set the cells amount
        for (int i = 0; i < height; i++) {
            String line = in.nextLine();
            char[] cArray = line.toCharArray();
            for (int j = 0; j < width; j++) {
                if ('.' != cArray[j]) {
                    Cell c = nodes.get(j).get(i);
                    System.err.println(String.format("Found node:%s = %s", c, cArray[j]));
                    c.setAmount(cArray[j]);
                    cells.add(c);
                    c.addPossibleLink(up[j]);
                    c.addPossibleLink(last);
                    up[j] = c;
                    last = c;
                }
            }
            last = null;
        }
        // links the nodes together and put them in the priority queue
        return new CustomQueue(cells);
    }

    /**
     * Init each cells of the grids and puts it in the map (x then y)
     */
    private static Map<Integer, Map<Integer, Cell>> intiMap(int height, int width) {
        Map<Integer, Map<Integer, Cell>> nodes = new HashMap<>();
        for (int i = 0; i < width; i++) {
            Map<Integer, Cell> subMap = new HashMap<>((int) (1.5 * height));
            nodes.put(i, subMap);
            for (int j = 0; j < height; j++) {
                subMap.put(j, new Cell(i, j));
            }
        }
        return nodes;
    }

    static class Cell {
        int x;
        int y;
        boolean isNode = false;
        int initAmount = 0;
        int amountNeeded = 0;
        Map<Cell, Integer> possibleLinks = new HashMap<>(6);
        boolean useForHLink = false;
        boolean useForVLink = false;

        public Cell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        void setAmount(char amountNeeded) {
            this.isNode = true;
            this.amountNeeded = Character.getNumericValue(amountNeeded);
            this.initAmount = this.amountNeeded;
        }

        boolean needsLink() {
            return isNode && amountNeeded > 0;
        }

        int canLinkTo(Cell other) {
            int out;
            if(possibleLinks.size() > 1 && initAmount == 1 && other.initAmount == 1) {
                // cannot link to another 1 if there is another solution
                out = 0;
            } else {
                out = Math.min(Math.min(other.amountNeeded, amountNeeded), 2 - possibleLinks.get(other));
            }
            return out;
        }

        void addPossibleLink(Cell other) {
            if (other != null) {
                this.possibleLinks.put(other, 0);
                other.possibleLinks.put(this, 0);
            }
        }

        /**
         * update the amount on a link for this cell and the linked cell
         * update the total amount needed after the opreation
         */
        void addLink(Cell other, int qty) {
            this.computeQty(other, qty);
            other.computeQty(this, qty);
        }

        private void computeQty(Cell other, int qty) {
            int i = possibleLinks.get(other) + qty;
            possibleLinks.put(other, i);
            amountNeeded -= qty;
        }
        /**
         * is used to choose rapidly some easy cell to add links that cannot be elsewhere
         */
        boolean isSolved() {
            return possibleLinks.size() == 1 // only one solution
                    || possibleLinks.keySet().stream().mapToInt(this::canLinkTo).sum() == amountNeeded;
        }

        @Override
        public String toString() {
            return String.format("(%s, %s)", x, y);
        }
    }

    static class CustomQueue extends PriorityQueue<Cell> {
        CustomQueue(Collection<Cell> cells) {
            super((c1, c2) -> {
                int out = c1.isSolved() ? -1 : (c2.isSolved() ? +1 : 0);
                if (out == 0) {
                    out = Integer.compare(c1.initAmount, c2.initAmount);
                }
                if (out == 0) {
                    out = Integer.compare(c2.amountNeeded, c1.amountNeeded);
                }
                return out;
            });
            addAll(cells);
        }

        @Override
        public boolean add(Cell cell) {
            boolean out = false;
            if (cell.needsLink()) {
                System.err.println(String.format("%s put in queue, amount=%s", cell, cell.amountNeeded));
                out = super.add(cell);
            } else {
                System.err.println(String.format("%s finished", cell));
            }
            return out;
        }
    }
}