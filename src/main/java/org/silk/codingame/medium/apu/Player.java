package org.silk.codingame.medium.apu;

import java.util.Scanner;

/**
 * Don't let the machines win. You are humanity's last hope...
 */
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int width = in.nextInt(); // the number of cells on the X axis
        in.nextLine();
        int height = in.nextInt(); // the number of cells on the Y axis
        in.nextLine();
        boolean[][] grid = new boolean[width][height];
        for (int i = 0; i < height; i++) {
            String line = in.nextLine(); // width characters, each either 0 or .
            char[] cArray = line.toCharArray();
            for (int j = 0; j < width; j++) {
                if ('0' == cArray[j]) {
                    System.err.println("Found node:" + j + "/" + i);
                    grid[j][i] = true;
                } else {
                    grid[j][i] = false;
                }
            }
        }
        for (int x1 = 0; x1 < width; x1++) {
            for (int y1 = 0; y1 < height; y1++) {
                System.err.println(x1 + "/" + y1 + "=" + grid[x1][y1]);
                if (grid[x1][y1]) {
                    System.err.println("Found node");
                    StringBuilder sb = new StringBuilder().append(x1).append(" ").append(y1).append(" ");
                    System.err.println("1:" + sb);
                    int x2 = x1 + 1;
                    while (x2 < width) {
                        if (grid[x2][y1]) {
                            System.err.println("Found x node");
                            break;
                        }
                        x2++;
                    }
                    if (x2 == width) {
                        System.err.println("Could not find x node");
                        sb.append("-1 -1");
                    } else {
                        sb.append(x2).append(" ").append(y1);
                    }
                    System.err.println("2:" + sb);
                    sb.append(" ");
                    int y2 = y1 + 1;
                    while (y2 < height) {
                        if (grid[x1][y2]) {
                            System.err.println("Found y node");
                            break;
                        }
                        y2++;
                    }
                    if (y2 == height) {
                        System.err.println("Could not find y node");
                        sb.append("-1 -1");
                    } else {
                        sb.append(x1).append(" ").append(y2);
                    }
                    System.err.println("3:" + sb);
                    System.out.println(sb);
                }
            }
        }
    }
}