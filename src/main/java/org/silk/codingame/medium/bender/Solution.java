package org.silk.codingame.medium.bender;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Bender bender = new Bender();
        bender.buildMap();
        List<String> movements = bender.move();
        movements.forEach(System.out::println);
    }

    private enum Element {
        SOUTH, EAST, NORTH, WEST,
        TELEPORT,
        BEER,
        NON_BREAKABLE, BREAKABLE,
        INVERTER,
        SUICIDE,
        NONE
    }

    private static class Bender {
        Element[][] map;
        // bender position
        int startX = -1;
        int startY = -1;
        // teleport 1
        int t1X = -1;
        int t1Y = -1;
        // teleport 2
        int t2X = -1;
        int t2Y = -1;

        Element currentDirection = Element.SOUTH;
        // current state
        boolean breakerMode = false;
        boolean inverted = false;
        boolean end = false;
        // to check loop
        Set<Movement> moves = new HashSet<>();
        // output
        List<String> movements = new ArrayList<>();

        void buildMap() {
            Scanner in = new Scanner(System.in);
            int l = in.nextInt();
            int c = in.nextInt();
            in.nextLine();
            map = new Element[c][l];

            for (int i = 0; i < l; i++) {
                String row = in.nextLine();
                char[] chars = row.toCharArray();
                for (int j = 0; j < chars.length; j++) {
                    char ca = chars[j];
                    Element current = charToElement(ca, i, j);
                    map[j][i] = current;
                }
            }
        }

        Element charToElement(char ca, int i, int j) {
            Element current;
            switch (ca) {
                case '#':
                    current = Element.NON_BREAKABLE;
                    break;
                case 'X':
                    current = Element.BREAKABLE;
                    break;
                case '$':
                    current = Element.SUICIDE;
                    break;
                case 'I':
                    current = Element.INVERTER;
                    break;
                case 'B':
                    current = Element.BEER;
                    break;
                case 'S':
                    current = Element.SOUTH;
                    break;
                case 'E':
                    current = Element.EAST;
                    break;
                case 'N':
                    current = Element.NORTH;
                    break;
                case 'W':
                    current = Element.WEST;
                    break;
                case '@':
                    current = Element.NONE;
                    startX = j;
                    startY = i;
                    break;
                case 'T':
                    current = Element.TELEPORT;
                    if (t1X == -1) {
                        t1X = j;
                        t1Y = i;
                    } else {
                        t2X = j;
                        t2Y = i;
                    }
                    break;
                default:
                    current = Element.NONE;
                    break;
            }
            System.err.println(String.format("(%s,%s) %s=>%s", j, i, ca, current));
            return current;
        }

        List<String> move() {
            while (!end) {
                checkLoop();
                updateBenderFromCurrentPosition();
                if (!end) {
                    moveToNext();
                }
            }
            return movements;
        }

        void checkLoop() {
            Movement move = new Movement(startX, startY, currentDirection, breakerMode, inverted);
            if (!moves.add(move)) {
                end = true;
                movements.forEach(System.err::println);
                movements.clear();
                movements.add("LOOP");
            }
        }

        void moveToNext() {
            for (Element direction : getDirections()) {
                int futurX = startX;
                int futurY = startY;
                switch (direction) {
                    case SOUTH:
                        futurY++;
                        break;
                    case EAST:
                        futurX++;
                        break;
                    case NORTH:
                        futurY--;
                        break;
                    case WEST:
                        futurX--;
                        break;
                }
                Element futur = map[futurX][futurY];
                System.err.println(String.format("Futur (%s,%s) %s %s", futurX, futurY, direction, futur));
                if (futur == Element.NON_BREAKABLE || (futur == Element.BREAKABLE && !breakerMode)) {
                    System.err.println("Rotate");
                } else {
                    System.err.println(String.format("Move from (%s,%s) to (%s,%s)", startX, startY, futurX, futurY));
                    startX = futurX;
                    startY = futurY;
                    currentDirection = direction;
                    movements.add(currentDirection.toString());
                    break;
                }
            }
        }

        protected List<Element> getDirections() {
            List<Element> toTest = new ArrayList<>(5);
            toTest.add(currentDirection);
            if (inverted) {
                toTest.add(Element.WEST);
                toTest.add(Element.NORTH);
                toTest.add(Element.EAST);
                toTest.add(Element.SOUTH);
            } else {
                toTest.add(Element.SOUTH);
                toTest.add(Element.EAST);
                toTest.add(Element.NORTH);
                toTest.add(Element.WEST);
            }
            return toTest;
        }

        void updateBenderFromCurrentPosition() {
            Element current = map[startX][startY];
            System.err.println(String.format("(%s,%s) %s %s", startX, startY, currentDirection, current));
            switch (current) {

                case SOUTH:
                case EAST:
                case NORTH:
                case WEST:
                    currentDirection = current;
                    break;

                case SUICIDE:
                    System.err.println("Suicide");
                    end = true;
                    break;
                case BEER:
                    breakerMode = !breakerMode;
                    System.err.println("Breaker Mode switch to " + (breakerMode ? "on" : "off"));
                    break;
                case INVERTER:
                    inverted = !inverted;
                    System.err.println("Inverted Mode switch to " + (inverted ? "on" : "off"));
                    break;

                case BREAKABLE:
                    breakableWall();
                    break;
                case NON_BREAKABLE:
                    System.err.println("error should not happen");
                    break;

                case TELEPORT:
                    teleport();
                    break;
            }
        }

        private void breakableWall() {
            if (breakerMode) {
                System.err.println(String.format("Broke (%s,%s)", startX, startY));
                map[startX][startY] = Element.NONE;
                moves.clear();
            } else {
                System.err.println("error should not happen");
            }
        }

        private void teleport() {
            if (startX == t1X && startY == t1Y) {
                startX = t2X;
                startY = t2Y;
            } else if (startX == t2X && startY == t2Y) {
                startX = t1X;
                startY = t1Y;
            } else {
                System.err.println("error should not happen");
            }
            System.err.println(String.format("Teleport to (%s,%s)", startX, startY));
        }
    }

    private static class Movement {
        int x;
        int y;
        Element direction;
        boolean breakerMode;
        boolean inverted;

        Movement(int x, int y, Element direction, boolean breakerMode, boolean inverted) {
            this.x = x;
            this.y = y;
            this.breakerMode = breakerMode;
            this.direction = direction;
            this.inverted = inverted;
        }

        @Override
        public boolean equals(Object o) {
            boolean eq = false;
            if (o instanceof Movement) {
                Movement m = (Movement) o;
                eq = m.x == x && m.y == y && m.direction == direction && m.breakerMode == breakerMode && m.inverted == inverted;
                if (eq) {
                    System.err.println(String.format("(%s,%s) Loop (dir=%s, break=%s, inv=%s)", x, y, direction, breakerMode, inverted));
                }
            }
            return eq;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            result = 31 * result + direction.hashCode();
            result = 31 * result + (breakerMode ? 1 : 0);
            result = 31 * result + (inverted ? 1 : 0);
            return result;
        }
    }
}