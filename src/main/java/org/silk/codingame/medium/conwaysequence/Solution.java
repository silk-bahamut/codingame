package org.silk.codingame.medium.conwaysequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int r = in.nextInt();
        int l = in.nextInt();

        List<Integer> out = Arrays.asList(r);
        for (int i = 0; i < l - 1; i++) {
            out = computeNextLine(out);
            System.err.println(out.stream().map(Object::toString).collect(Collectors.joining(" ")));
        }
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(out.stream().map(Object::toString).collect(Collectors.joining(" ")));
    }

    static List<Integer> computeNextLine(List<Integer> lst) {
        List<Integer> newLst = new ArrayList<>();
        Integer last = null;
        int count = 0;
        for (int i : lst) {
            if (last == null) {
                last = i;
            }
            if (last == i) {
                count++;
            } else {
                newLst.add(count);
                newLst.add(last);
                count = 1;
                last = i;
            }
        }
        newLst.add(count);
        newLst.add(last);
        return newLst;
    }
}