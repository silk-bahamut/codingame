package org.silk.codingame.medium.dwarfs;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Map<Integer, Set<Integer>> links = computeLinks();
        System.err.println(links);
        Set<Integer> roots = findRoots(links);
        int longest = roots.stream().mapToInt(root -> getLongest(root, links)).max().getAsInt();
        System.out.println(longest); // The number of people involved in the longest succession of influences
    }

    private static int getLongest(Integer root, Map<Integer, Set<Integer>> links) {
        System.err.println("Root : " + root);
        Set<Integer> toAnalyze = Collections.singleton(root);

        int longest = 0;
        while (!toAnalyze.isEmpty()) {
            final Set<Integer> nextToAnalyze = new HashSet<>();
            toAnalyze.stream()
                    .map(links::get)
                    .filter(s -> s != null)
                    .forEach(nextToAnalyze::addAll);
            toAnalyze = nextToAnalyze;
            longest++;
        }
        return longest;
    }

    private static Map<Integer, Set<Integer>> computeLinks() {
        Map<Integer, Set<Integer>> links = new HashMap<>();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the number of relationships of influence
        for (int i = 0; i < n; i++) {
            int x = in.nextInt(); // a relationship of influence between two people (x influences y)
            int y = in.nextInt();
            Set<Integer> set = links.get(x);
            if (set == null) {
                set = new HashSet<>();
                links.put(x, set);
            }
            set.add(y);
        }
        return links;
    }

    private static Set<Integer> findRoots(Map<Integer, Set<Integer>> links) {
        final Set<Integer> set = new HashSet<>(links.keySet());
        links.values().stream().forEach(set::removeAll);
        return set;
    }
}