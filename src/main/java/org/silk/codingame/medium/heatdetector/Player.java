package org.silk.codingame.medium.heatdetector;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int w = in.nextInt(); // width of the building.
        int h = in.nextInt(); // height of the building.
        int n = in.nextInt(); // maximum number of turns before game over.
        int x0 = in.nextInt();
        int y0 = in.nextInt();

        // game loop
        int minX = 0;
        int minY = 0;
        int maxX = w - 1;
        int maxY = h - 1;
        while (true) {
            String bOMBDIR = in.next(); // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
            System.err.println(bOMBDIR);

            if (bOMBDIR.contains("R")) {
                minX = x0;
                System.err.println("x :" + minX + ">" + maxX);
                x0 += Math.round(((double) maxX - minX) / 2.0);
            } else if (bOMBDIR.contains("L")) {
                maxX = x0;
                System.err.println("x :" + minX + ">" + maxX);
                x0 -= Math.round(((double) maxX - minX) / 2.0);
            }
            if (bOMBDIR.contains("D")) {
                minY = y0;
                System.err.println("y :" + minY + ">" + maxY);
                y0 += Math.round(((double) maxY - minY) / 2.0);
            } else if (bOMBDIR.contains("U")) {
                maxY = y0;
                System.err.println("y :" + minY + ">" + maxY);
                y0 -= Math.round(((double) maxY - minY) / 2.0);
            }
            System.out.println(x0 + " " + y0);
        }
    }
}