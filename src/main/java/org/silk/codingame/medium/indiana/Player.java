package org.silk.codingame.medium.indiana;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int w = in.nextInt(); // number of columns.
        int h = in.nextInt(); // number of rows.
        in.nextLine();
        int[][] map = new int[w][h];
        for (int i = 0; i < h; i++) {
            String lINE = in.nextLine(); // represents a line in the grid and contains W integers. Each integer represents one room of a given type.
            String[] s = lINE.split(" ");
            for (int j = 0; j < s.length; j++) {
                map[j][i] = Integer.parseInt(s[j]);
            }
        }
        int eX = in.nextInt(); // the coordinate along the X axis of the exit (not useful for this first mission, but must be read).
        in.nextLine();

        // game loop
        while (true) {
            int xI = in.nextInt();
            int yI = in.nextInt();
            String pOS = in.next();
            in.nextLine();

            int current = map[xI][yI];
            switch (current) {
                case 1:
                case 3:
                case 7:
                case 8:
                case 9:
                case 12:
                case 13:
                    yI++;
                    break;
                case 2:
                case 6:
                    if ("LEFT".equals(pOS)) {
                        xI++;
                    } else {
                        xI--;
                    }
                    break;
                case 4:
                case 10:
                    if ("TOP".equals(pOS)) {
                        xI--;
                    } else {
                        yI++;
                    }
                    break;
                case 5:
                case 11:
                    if ("TOP".equals(pOS)) {
                        xI++;
                    } else {
                        yI++;
                    }
                    break;
                default:
                    System.err.println("0, should not happen");
                    break;
            }
            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");

            System.out.println(xI + " " + yI);
        }
    }
}
