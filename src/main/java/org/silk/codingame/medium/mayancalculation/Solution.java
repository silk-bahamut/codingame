package org.silk.codingame.medium.mayancalculation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int L = in.nextInt();
        int H = in.nextInt();
        List<Number> lst = new ArrayList<>(20);
        for (int i = 0; i < H; i++) {
            String numeral = in.next();
            //System.err.println(i + ": numeral=" + numeral);
            boolean first = lst.isEmpty();
            for (int j = 0; j * L < numeral.length(); j++) {
                Number n;
                if (first) {
                    n = new Number(H);
                    lst.add(n);
                } else {
                    n = lst.get(j);
                }
                String sub = numeral.substring(j * L, (j + 1) * L);
                //System.err.println(String.format("(%s,%s) %s", i, j, sub));
                n.representation.add(sub);
            }
        }
        //System.err.println("lst=" + lst);
        long a = extractNumber(in, H, lst);
        System.err.println("a=" + a);
        long b = extractNumber(in, H, lst);
        System.err.println("b=" + b);
        String operation = in.next();
        long out = 0;
        if ("+".equals(operation)) {
            out = a + b;
        } else if ("*".equals(operation)) {
            out = a * b;
        } else if ("/".equals(operation)) {
            out = a / b;
        } else if ("-".equals(operation)) {
            out = a - b;
        }
        System.err.println("out=" + out);
        List<Long> restes = new ArrayList<>();
        boolean bo = true;
        while (bo) {
            long reste = out % 20;
            long div = out / 20;
            bo = div != 0;
            restes.add(reste);
            out = div;
        }
        Collections.reverse(restes);
        System.err.println("restes=" + restes);
        for (long reste : restes) {
            Number n = lst.get((int) reste);
            n.representation.forEach(System.out::println);
        }
    }

    private static long extractNumber(Scanner in, int H, List<Number> lst) {
        int S = in.nextInt();
        int size = S / H;
        List<Number> num = new ArrayList<>(S / H);
        for (int i = 0; i < size; i++) {
            Number n = new Number(H);
            num.add(n);
            for (int j = 0; j < H; j++) {
                String num1Line = in.next();
                n.representation.add(num1Line);
            }
        }
        long out = 0;
        for (Number number : num) {
            out *= 20;
            for (int i = 0; i < 20; i++) {
                Number n = lst.get(i);
                if (n.equals(number)) {
                    out += i;
                    break;
                }
            }
        }
        return out;
    }

    static class Number {
        List<String> representation;

        public Number(int size) {
            this.representation = new ArrayList<>(size);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("Number  {\n");
            for (String s : representation) {
                sb.append(s).append("\n");
            }
            sb.append("}");
            return sb.toString();
        }

        @Override
        public boolean equals(Object o) {
            Number n = (Number) o;
            boolean out = true;
            for (int i = 0; i < representation.size(); i++) {
                if (!representation.get(i).equals(n.representation.get(i))) {
                    out = false;
                    break;
                }
            }
            return out;
        }
    }
}