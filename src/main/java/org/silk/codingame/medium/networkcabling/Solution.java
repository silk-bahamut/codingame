package org.silk.codingame.medium.networkcabling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> lst = new ArrayList<>();
        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            int x = in.nextInt();
            int y = in.nextInt();
            int count = 1;
            lst.add(y);
            if (map.containsKey(y)) {
                count += map.get(y);
            }
            map.put(y, count);
            minX = Math.min(minX, x);
            minY = Math.min(minY, y);
            maxX = Math.max(maxX, x);
            maxY = Math.max(maxY, y);
        }

        System.err.println(String.format("X : %s to %s", minX, maxX));
        System.err.println(String.format("Y : %s to %s", minY, maxY));
        System.err.println(String.format("Map size : %s", map.size()));

        double median = computeMedian(lst);
        long out = computeSize(median, map);
        out += maxX - minX;
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(out);
    }

    static double computeMedian(List<Integer> lst) {
        Collections.sort(lst);
        int size = lst.size();
        double median;
        if (size % 2 == 0) {
            median = ((double) lst.get(size / 2) + (double) lst.get(size / 2 - 1)) / 2;
        } else {
            median = (double) lst.get(size / 2);
        }
        System.err.println(String.format("Median : %s", median));
        return median;
    }

    static long computeSize(Double avg, Map<Integer, Integer> map) {
        final int tBis = avg.intValue();
        long sum = map.entrySet().parallelStream().mapToLong(entry -> entry.getValue() * Math.abs(tBis - entry.getKey())).sum();
        System.err.println(String.format("%s=%s", tBis, sum));
        return sum;
    }
}