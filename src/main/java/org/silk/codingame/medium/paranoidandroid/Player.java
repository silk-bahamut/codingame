import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int nbFloors = in.nextInt(); // number of floors
        int width = in.nextInt(); // width of the area
        int nbRounds = in.nextInt(); // maximum number of rounds
        int exitFloor = in.nextInt(); // floor on which the exit is found
        int exitPos = in.nextInt(); // position of the exit on its floor
        int nbTotalClones = in.nextInt(); // number of generated clones
        int nbAdditionalElevators = in.nextInt(); // ignore (always zero)
        int nbElevators = in.nextInt(); // number of elevators
        Map<Integer, Integer> elevPos = new HashMap<>();
        for (int i = 0; i < nbElevators; i++) {
            int elevatorFloor = in.nextInt(); // floor on which this elevator is found
            int elevatorPos = in.nextInt(); // position of the elevator on its floor
            System.err.println("Elevator of floor " + elevatorFloor + ":" + elevatorPos);
            elevPos.put(elevatorFloor, elevatorPos);
        }
        elevPos.put(exitFloor, exitPos);

        // game loop
        Integer lastDistance = null;
        while (true) {
            int cloneFloor = in.nextInt(); // floor of the leading clone
            int clonePos = in.nextInt(); // position of the leading clone on its floor
            String direction = in.next(); // direction of the leading clone: LEFT or RIGHT
            System.err.println("Position clone : " + clonePos);
            System.err.println("Floor clone : " + cloneFloor);

            System.err.println("Last distance : " + lastDistance);
            System.err.println("Elevator : " + elevPos.get(cloneFloor));
            if (clonePos == -1) {
                lastDistance = null;
                System.err.println("No clone");
                System.out.println("WAIT");
            } else {
                int newDistance = Math.abs(clonePos - elevPos.get(cloneFloor));
                if (newDistance == 0) {
                    // on the elevator
                    if (cloneFloor != nbFloors - 1) {
                        lastDistance = Math.abs(clonePos - elevPos.get(cloneFloor + 1));
                    } else {
                        lastDistance = null;
                    }
                    System.err.println("Waiting lift");
                    System.out.println("WAIT");
                } else {
                    if (lastDistance != null && newDistance > lastDistance) {
                        System.err.println("Wrong direction");
                        // s'eloigne de l'ascenseur
                        System.out.println("BLOCK");
                    } else {
                        System.err.println("Right direction");
                        System.out.println("WAIT");
                    }
                    lastDistance = newDistance;
                }
            }
        }
    }
}