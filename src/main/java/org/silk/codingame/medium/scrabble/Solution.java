package org.silk.codingame.medium.scrabble;

import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {
    static class Word implements Comparable<Word> {
        char[] chars;
        String word;
        int points;
        int position;

        public Word(int position, String word) {
            this.word = word;
            this.chars = word.toCharArray();
            this.position = position;
            this.points = word.chars().map(c -> {
                switch (c) {
                    case 'd':
                    case 'g':
                        return 2;
                    case 'b':
                    case 'c':
                    case 'm':
                    case 'p':
                        return 3;
                    case 'f':
                    case 'h':
                    case 'v':
                    case 'w':
                    case 'y':
                        return 4;
                    case 'k':
                        return 5;
                    case 'j':
                    case 'x':
                        return 8;
                    case 'q':
                    case 'z':
                        return 10;
                    default:
                        return 1;
                }
            }).sum();
        }

        @Override
        public String toString() {
            return word + "=" + points;
        }

        @Override
        public int compareTo(Word o) {
            int out = Integer.compare(o.points, points);
            if (out == 0) {
                out = Integer.compare(position, o.position);
            }
            return out;
        }

        public boolean canSolve(String letters) {
            char[] ls = letters.toCharArray();
            boolean out = true;
            for (char w : chars) {
                int position = -1;
                for (int i = 0; i < ls.length; i++) {
                    if (ls[i] == w) {
                        position = i;
                        break;
                    }
                }
                if (position == -1) {
                    out = false;
                    break;
                } else {
                    ls[position] = '@';
                }
            }
            return out;
        }
    }


    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.nextLine();
        PriorityQueue<Word> dico = new PriorityQueue<>();
        for (int i = 0; i < n; i++) {
            String word = in.nextLine();
            dico.add(new Word(i, word));
        }
        String letters = in.nextLine();
        Word w;
        do {
            w = dico.poll();
            System.err.println(w);
        } while (!dico.isEmpty() && !w.canSolve(letters));
        System.out.println(w.word);
    }
}