package org.silk.codingame.medium.skynet;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the total number of nodes in the level, including the gateways
        Map<Integer, Node> map = new HashMap<>();

        for (int i = 0; i < n; i++) {
            map.put(i, new Node(i));
        }
        int l = in.nextInt(); // the number of links
        int e = in.nextInt(); // the number of exit gateways
        for (int i = 0; i < l; i++) {
            int n1 = in.nextInt(); // N1 and N2 defines a link between these nodes
            int n2 = in.nextInt();
            Node origin = map.get(n1);
            Node destination = map.get(n2);
            origin.addLink(destination);
            destination.addLink(origin);
        }

        for (int i = 0; i < e; i++) {
            int eI = in.nextInt(); // the index of a gateway node
            map.get(eI).gateWay = true;
            System.err.println("New Gateway " + eI);
        }

        // game loop

        while (true) {
            int sI = in.nextInt(); // The index of the node on which the Skynet agent is positioned this turn

            Node skyNet = map.get(sI);
            Optional<Node> op = skyNet.links.stream().filter(Node::isGateWay).findAny();
            if (op.isPresent()) {
                System.err.println("Found a gateway near skynet");
                Node node = op.get();
                skyNet.links.remove(node);
                node.links.remove(skyNet);

                System.out.println(sI + " " + node.index);
            } else {
                System.err.println("Looking for parent of gateway");
                op = map.values().stream().filter(Node::isGateWay).filter(Node::isLinked).findAny();
                if (op.isPresent()) {
                    System.err.println("Found gateway removing parent");
                    Node node = op.get();
                    Node toKill = node.links.stream().findAny().get();
                    toKill.links.remove(node);
                    node.links.remove(toKill);
                    System.out.println(toKill.index + " " + node.index);
                } else {
                    System.err.println("Fails finding a node near gateway");
                }
            }
        }
    }

    private static void displayLinks(String name, Node n) {
        System.err.println(name + " : " + n.links.stream().map(Node::getIndex).collect(Collectors.joining(",")));
    }

    private static class Node {
        int index;
        boolean gateWay = false;
        Set<Node> links = new HashSet<>();

        Node(int i) {
            index = i;
        }

        boolean isLinked() {
            return !links.isEmpty();
        }

        void addLink(Node node) {
            links.add(node);
        }

        boolean isGateWay() {
            return gateWay;
        }

        String getIndex() {
            return "" + index;
        }

        public boolean equals(Object o) {
            return ((Node) o).index == this.index;
        }

        public int hashCode() {
            return index;
        }
    }
}