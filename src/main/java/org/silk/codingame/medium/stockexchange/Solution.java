import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.nextLine();
        String vs = in.nextLine();
        List<Integer> lst = new ArrayList<>();

        Integer max = null;
        Integer min = null;
        int oldDelta = 0;
        for (String str : vs.split(" ")) {
            int i = Integer.parseInt(str);
            if (max == null) {
                max = i;
            } else {
                if (max > i) {
                    if (min == null || i < min) {
                        min = i;
                    }
                } else {
                    if (min != null) {
                        oldDelta = Math.min(oldDelta, min - max);
                    }
                    max = i;
                    min = null;
                }
            }
        }
        if (min != null) {
            oldDelta = Math.min(oldDelta, min - max);
        }
        System.out.println(oldDelta);
    }
}