package org.silk.codingame.medium.teads;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the number of adjacency relations
        // init first node
        Map<Integer, Set<Integer>> links = populateLinks(in, n);

        Integer leaf = links.entrySet()
                .parallelStream()
                .filter(e -> e.getValue().size() == 1) // is only linked to one other element
                .findFirst() // need only one
                .get() // there will always be some leafs
                .getKey(); // we need the number to continue

        int propag = 0;
        Set<Integer> persons = new HashSet<>();
        Set<Integer> toAnalyze = Collections.singleton(leaf);
        while (persons.size() != links.size()) {
            Set<Integer> nextToAnalyze = new HashSet<>();
            for(Integer i : toAnalyze) {
                persons.add(i);
                nextToAnalyze.addAll(links.get(i));
            }
            nextToAnalyze.removeAll(persons);
            toAnalyze = nextToAnalyze;
            propag++;
        }
        // use propag - 1 because we are always one loop to high since the last always have only already checked elements
        System.out.println((propag % 2 == 0) ? propag / 2 : (propag - 1) / 2);
    }

    private static Map<Integer, Set<Integer>> populateLinks(Scanner in, int n) {
        Map<Integer, Set<Integer>> links = new HashMap<>();

        for (int i = 0; i < n; i++) {
            int xi = in.nextInt(); // the ID of a person which is adjacent to yi
            int yi = in.nextInt(); // the ID of a person which is adjacent to xi
            addToLinks(links, xi, yi);
            addToLinks(links, yi, xi);
        }
        return links;
    }

    private static void addToLinks(Map<Integer, Set<Integer>> links, int xi, int yi) {
        Set<Integer> set = links.get(xi);
        if (set == null) {
            set = new HashSet<>();
            links.put(xi, set);
        }
        set.add(yi);
    }

}