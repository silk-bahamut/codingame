package org.silk.codingame.medium.telephonenumber;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {
    static int entries = 0;

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Map numbers = new HashMap();
        Queue<Character> q = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            String telephone = in.next();
            q.clear();
            for (char c : telephone.toCharArray()) {
                q.add(c);
            }
            addNumber(q, numbers);
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(entries); // The number of elements (referencing a number) stored in the structure.
    }

    private static void addNumber(Queue<Character> q, Map map) {
        Character c = q.poll();
        Map subMap = (Map) map.get(c);
        if (subMap == null) {
            subMap = new HashMap();
            map.put(c, subMap);
            entries++;
        }
        if (!q.isEmpty()) {
            addNumber(q, subMap);
        }

    }

}