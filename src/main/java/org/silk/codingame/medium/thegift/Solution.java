package org.silk.codingame.medium.thegift;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int C = in.nextInt();
        List<Participant> participants = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            int B = in.nextInt();
            Participant p = new Participant(i, B);
            participants.add(p);
        }
        Collections.sort(participants, (p1, p2) -> Integer.compare(p1.maxCotisation, p2.maxCotisation));
        int budgetise = recur(participants, 0, C, C);
        System.err.println("budgetise=" + budgetise);
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");
        if (budgetise == C) {
            participants.stream()
                    .sorted((p1, p2) -> Integer.compare(p1.actualCotisation, p2.actualCotisation))
                    .forEach(p -> System.out.println(p.actualCotisation));
        } else {
            System.out.println("IMPOSSIBLE");
        }
    }

    public static int recur(List<Participant> participants, int budgetise, int total, int reste) {
        int avg = reste / participants.size();
        System.err.println(String.format("%s participants for %s with an avg of %s", participants.size(), total, avg));
        avg = avg == 0 ? 1 : avg;
        List<Participant> notMaxed = new ArrayList<>();
        for (Participant p : participants) {
            int actual = p.setCotisation(avg);
            budgetise += actual;
            System.err.println(String.format("add %s to part %s, budgetise=%s", actual, p.position, budgetise));
            if (budgetise >= total) {
                System.err.println("total atteint, break");
                break;
            }
            if (actual == avg) {
                notMaxed.add(p);
            }
        }
        if (budgetise < total && !notMaxed.isEmpty()) {
            budgetise = recur(notMaxed, budgetise, total, total - budgetise);
        }
        return budgetise;
    }

    static class Participant {
        final int position;
        final int maxCotisation;
        int actualCotisation = 0;

        public Participant(int position, int maxCotisation) {
            this.position = position;
            this.maxCotisation = maxCotisation;
        }

        public int setCotisation(int excepted) {
            if (excepted + actualCotisation > maxCotisation) {
                excepted = maxCotisation - actualCotisation;
            }
            actualCotisation += excepted;
            return excepted;
        }
    }
}