package org.silk.codingame.medium.winamax;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 */
class Solution {
    static Map<String, Integer> map = new HashMap<>();

    static {
        for (int i = 2; i < 11; i++) {
            map.put("" + i, i);
        }
        map.put("J", 11);
        map.put("Q", 12);
        map.put("K", 13);
        map.put("A", 14);
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        Queue<String> deck1 = new LinkedList<>();
        Queue<String> deck2 = new LinkedList<>();
        int n = in.nextInt(); // the number of cards for player 1
        for (int i = 0; i < n; i++) {
            String cardp1 = in.next(); // the n cards of player 1
            deck1.add(cardp1);
        }
        int m = in.nextInt(); // the number of cards for player 2
        for (int i = 0; i < m; i++) {
            String cardp2 = in.next(); // the m cards of player 2
            deck2.add(cardp2);
        }

        int turns = 0;
        List<String> war1 = new ArrayList();
        List<String> war2 = new ArrayList();
        boolean pat = false;
        while (!deck1.isEmpty() && !deck2.isEmpty()) {
            String caP1 = deck1.poll();
            war1.add(caP1);
            String caP2 = deck2.poll();
            war2.add(caP2);
            int comp = compare(caP1, caP2);
            if (comp == 0) {
                for (int i = 0; i < 3; i++) {
                    String c1 = deck1.poll();
                    String c2 = deck2.poll();
                    if (c1 == null || c2 == null) {
                        pat = true;
                        System.out.println("PAT");
                        break;
                    } else {
                        war1.add(c1);
                        war2.add(c2);
                    }
                }
            } else {
                if (comp == 1) {
                    deck1.addAll(war1);
                    war1.clear();
                    deck1.addAll(war2);
                    war2.clear();
                } else {
                    deck2.addAll(war1);
                    war1.clear();
                    deck2.addAll(war2);
                    war2.clear();
                }
                turns++;
            }

        }
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");
        if (!pat) {
            System.out.println((deck1.isEmpty() ? 2 : 1) + " " + turns);
        }
    }

    private static int compare(String card1, String card2) {
        String e1 = card1.substring(0, card1.length() - 1);
        String e2 = card2.substring(0, card2.length() - 1);
        return map.get(e1).compareTo(map.get(e2));
    }
}